FROM openjdk:17-jdk-alpine
EXPOSE 8097
COPY target/jenkins.jar jenkins.jar
ENTRYPOINT ["java","-jar","/jenkins.jar"]